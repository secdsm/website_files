# Archives

## File system format

Each month for each year will need a file in the appropriate folder.  For example, from the web root, August 2016 would go in `archives/2016/08`.  The name of the file needs to be the same as the month directory with the file extension `.json` attached.  Once again, for August 2016 the full path with the file included would be `archives/2016/08/08.json`.

## Data file format

Each month file consists of a json file with a master json object.  This master json object has three top level keys: `name`, `abbreviation`, and `talks`.

`name` is a string for the month name.
`abbreviation` is a string for the month abbreviation.  Sometimes this is the same as the `name` key when the month name is short.
`talks` is an array of objects.  The objects have two keys: `presenter` and `presentation`.
`presenter` is an object with two keys: `name` and `bio`.
`presenter.name` is a string for the name of the presenter.
`presenter.bio` is a string for the biography of the presenter.  This field is optional.
`presentation` is an object with three keys: `title`, `description`, and `links`.
`presentation.title` is a string for the title of the presentation.
`presentation.description` is a string for the description of the presentation.  This field is optional.
`presentation.links` is an array of objects for links for files from the presentation.  Each object has two keys: `url` and `type`.
`url` is a string for the url for the file.
`type` is the type of the file.  This can be any font-awesome icon selector, but is generally one of the following options:
- github
- youtube
- file-pdf-o (for pdf files)
- file-excel-o (for excel files)

An example file is included for demonstration purposes:

```
{
  "name": "August",
  "abbreviation": "Aug",
    "talks": [
      {
        "presenter": {
          "name": "James Beal"
        },
        "presentation": {
          "title": "Moloch",
          "description": "I will be discussing Moloch, an open source tool developed by a small team at AOL, to handle scalable indexing of PCAP's into a backend database. First I will cover general architecture and basic/standard network configurations. Moloch is composed of three parts, the capture component, the elasticsearch database, and the webGUI Viewer. Then we will move on to a \"live\" demo of the software components, with a look at the actual packet capture and info available in the Viewer app. Wrapping up with search functions and if there is time, a quick look at the pcap in Wireshark as a comparison.",
          "links": [
          {
            "url": "https://youtube.com/watch?v=E-UFegg29-k",
            "type": "youtube"
          }
          ]
        }
      },
      {
        "presenter": {
          "name": "Nate Subra and Brandon Murphy",
          "bio": "Brandon Murphy is a network security practitioner in Des Moines, Iowa. Nate Subra is an Infosec practitioner with a love for automation. He doesn't believe in a silver bullet solutions, he believes in silver clips loaded by the right people. PowerShell, threat emulation, and breaking into the internet of things are his current hobbies. Nate currently works out of Des Moines, Iowa."
        },
        "presentation": {
          "title": "An Intro to Bro",
          "description": "Bro (bro.org) is becoming a very popular network security monitoring tool. This talk will cover the basics of running a bro instance, integration with external threat intelligence (via critical stack) and analyzing the logs with elastic search.",
          "links": [
          {
            "url": "https://www.youtube.com/watch?v=v0vmviZO6rs",
            "type": "youtube"
          }
          ]
        }
      },
      {
        "presenter": {
          "name": "Open Discussion"
        },
        "presentation": {
          "title": "SecKC and DEFCON trip recap",
          "description": "A small group of SecDSM attendees are headed out to Kansas City on Aug 9th! While we're there, we'll be giving a short talk about how SecDSM became economic threat actors for the lulz.",
          "links": [
          {
            "url": "https://youtu.be/6YIlFnvlMjU",
            "type": "youtube"
          }
          ]
        }
      }
    ]
}```
